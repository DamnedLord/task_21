// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Pawn.h"
#include "PlayerPawnBase.generated.h"

class UCameraComponent;
class ASnakeBase;
class AFood;
class AObstacle;

UCLASS()
class TASK_21_API APlayerPawnBase : public APawn
{
	GENERATED_BODY()

public:
	// Sets default values for this pawn's properties
	APlayerPawnBase();

	UPROPERTY(BlueprintReadWrite)
	UCameraComponent *PawnCamera;

	UPROPERTY(BlueprintReadWrite)
	ASnakeBase *SnakeActor;

	UPROPERTY(EditDefaultsOnly)
	TSubclassOf<ASnakeBase> SnakeActorClass;

	UPROPERTY(EditDefaultsOnly)
	TSubclassOf<AFood> FoodClass;

	UPROPERTY(EditDefaultsOnly)
	int Width;

	UPROPERTY(EditDefaultsOnly)
	int Height;

	UPROPERTY(BlueprintReadOnly)
	int Level;

	UPROPERTY(BlueprintReadOnly)
	int Remains;

	UPROPERTY(BlueprintReadOnly)
	int Score;

	UPROPERTY(BlueprintReadOnly)
	bool IsGameOver;

	UPROPERTY(EditDefaultsOnly)
	TSubclassOf<AObstacle> ObstacleClass;

	UPROPERTY()
	TArray<AObstacle *> ObstacleElements;

	static APlayerPawnBase *Pawn;
protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

	void CreateSnakeActor();
	
	UFUNCTION()
	void HandlePlayerVerticalInput(float value);

	UFUNCTION()
	void HandlePlayerHorizontalInput(float value);

	UFUNCTION()
	void HandlePlayerRestartInput();

	UFUNCTION()
	void SpawnFood();

	UFUNCTION()
	void AddScore(int value = 1);

	int CalculateLevelFoods(int level);

	UFUNCTION()
	void AddObstacle();
};

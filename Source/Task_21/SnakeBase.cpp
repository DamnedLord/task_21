// Fill out your copyright notice in the Description page of Project Settings.

#include "SnakeBase.h"
#include "SnakeElementBase.h"
#include <Engine/World.h>
#include "Interactable.h"
#include "PlayerPawnBase.h"

// Sets default values
ASnakeBase::ASnakeBase()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	ElementSize = 100.f;
	MovementSpeed = 10.f;
	LastMoveDirection = NextMoveDirection = EMovementDirection::DOWN;
}

// Called when the game starts or when spawned
void ASnakeBase::BeginPlay()
{
	Super::BeginPlay();
	SetActorTickInterval(MovementSpeed);

	PlayerPawn = APlayerPawnBase::Pawn;
	
	AddSnakeElement(4);
}

// Called every frame
void ASnakeBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	Move();
}

void ASnakeBase::AddSnakeElement(int ElementsNum /*= 1*/)
{
	for (auto i = ElementsNum; 0 < i; --i)
	{
		FVector newLocation(0);
		FIntVector cellLocation(0);

		if (1 == ElementsNum && SnakeElements.Num())
		{
			auto lastElement = SnakeElements.Last();
			newLocation = lastElement->GetActorLocation();
			cellLocation = lastElement->CellLocation;
		}
		else
		{
			newLocation.X = ElementSize * SnakeElements.Num();
			cellLocation.X = PlayerPawn->Width / 2;
			cellLocation.Y = PlayerPawn->Height / 2 + SnakeElements.Num();
		}

		FTransform newTransform(newLocation);
		auto newSnakeElement = GetWorld()->SpawnActor<ASnakeElementBase>(SnakeElementClass, newTransform);
		
		newSnakeElement->SnakeOwner = this;
		newSnakeElement->CellLocation = cellLocation;

		if (!SnakeElements.Add(newSnakeElement))
		{
			newSnakeElement->SetFirstElementType();
		}
	}
}

void ASnakeBase::Move()
{
	FVector MovementVector(0.f);
	FIntVector CellOffset(0);

	switch (LastMoveDirection = NextMoveDirection)
	{
	case EMovementDirection::UP:
		MovementVector.X += ElementSize;
		++CellOffset.Y;
		break;
	case EMovementDirection::DOWN:
		MovementVector.X -= ElementSize;
		--CellOffset.Y;
		break;
	case EMovementDirection::LEFT:
		MovementVector.Y -= ElementSize;
		--CellOffset.X;
		break;
	case EMovementDirection::RIGHT:
		MovementVector.Y += ElementSize;
		++CellOffset.X;
		break;
	default:
		break;
	}

	SnakeElements[0]->ToggleCollision();

	for (auto i = SnakeElements.Num() - 1; 0 < i; --i)
	{
		auto CurrentElement = SnakeElements[i];
		auto PrevElement = SnakeElements[i - 1];

		auto PrevLocation = PrevElement->GetActorLocation();
		CurrentElement->SetActorLocation(PrevLocation);

		CurrentElement->CellLocation = PrevElement->CellLocation;
	}

	SnakeElements[0]->AddActorWorldOffset(MovementVector);
	SnakeElements[0]->CellLocation += CellOffset;

	SnakeElements[0]->ToggleCollision();
}

void ASnakeBase::SnakeElementOverlap(ASnakeElementBase * OverlappedElement, AActor *Other)
{
	if (!IsValid(OverlappedElement))
		return;

	auto InteractableInterface = Cast<IInteractable>(Other);
	if (!InteractableInterface)
	{
		this->Destroy();
		PlayerPawn->IsGameOver = true;
		return;
	}

	InteractableInterface->Interact(this, OverlappedElement->IsHead);
}

void ASnakeBase::Reset()
{

}
// Fill out your copyright notice in the Description page of Project Settings.

#include "PlayerPawnBase.h"

#include <Camera/CameraComponent.h>
#include <Engine/World.h>
#include "SnakeBase.h"
#include <Components/InputComponent.h>
#include "Food.h"
#include <Engine.h>

#include <vector>
#include <numeric>
#include "SnakeElementBase.h"
#include "Obstacle.h"

APlayerPawnBase * APlayerPawnBase::Pawn = nullptr;

// Sets default values
APlayerPawnBase::APlayerPawnBase() : Level(1), Remains(CalculateLevelFoods(Level)), Score(0), IsGameOver(false)
{
 	// Set this pawn to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	PawnCamera = CreateDefaultSubobject<UCameraComponent>(TEXT("PawnCamera"));
	RootComponent = PawnCamera;
}

// Called when the game starts or when spawned
void APlayerPawnBase::BeginPlay()
{
	Super::BeginPlay();
	
	SetActorRotation(FRotator(-90, 0, 0));

	Pawn = this;

	CreateSnakeActor();
}

// Called every frame
void APlayerPawnBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

// Called to bind functionality to input
void APlayerPawnBase::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

	PlayerInputComponent->BindAxis("Vertical", this, &APlayerPawnBase::HandlePlayerVerticalInput);
	PlayerInputComponent->BindAxis("Horizontal", this, &APlayerPawnBase::HandlePlayerHorizontalInput);
	PlayerInputComponent->BindAction("Restart", EInputEvent::IE_Pressed, this, &APlayerPawnBase::HandlePlayerRestartInput);
}

void APlayerPawnBase::CreateSnakeActor()
{
	SnakeActor = GetWorld()->SpawnActor<ASnakeBase>(SnakeActorClass, FTransform());
	SnakeActor->PlayerPawn = this;

	SpawnFood();
}

void APlayerPawnBase::HandlePlayerVerticalInput(float value)
{
	if (!IsValid(SnakeActor))
		return;

	if (value > 0 && SnakeActor->LastMoveDirection != EMovementDirection::DOWN)
		SnakeActor->NextMoveDirection = EMovementDirection::UP;
	if (value < 0 && SnakeActor->LastMoveDirection != EMovementDirection::UP)
		SnakeActor->NextMoveDirection = EMovementDirection::DOWN;
}

void APlayerPawnBase::HandlePlayerHorizontalInput(float value)
{
	if (!IsValid(SnakeActor))
		return;

	if (value < 0 && SnakeActor->LastMoveDirection != EMovementDirection::LEFT)
		SnakeActor->NextMoveDirection = EMovementDirection::RIGHT;
	if (value > 0 && SnakeActor->LastMoveDirection != EMovementDirection::RIGHT)
		SnakeActor->NextMoveDirection = EMovementDirection::LEFT;
}

void APlayerPawnBase::HandlePlayerRestartInput()
{
	UGameplayStatics::OpenLevel(this, FName(*GetWorld()->GetName()), false);
}

void APlayerPawnBase::SpawnFood()
{
	if (IsValid(SnakeActor))
	{
		if (!--Remains)
		{
			++Level;
			Remains = CalculateLevelFoods(Level);

			SnakeActor->Reset();
			AddObstacle();

			SnakeActor->MovementSpeed *= 0.90f;
			SnakeActor->SetActorTickInterval(FMath::Max(SnakeActor->MovementSpeed, SnakeActor->MaximumMovementSpeed));
		}

		static int yOffset = -1 * Height / 2;
		static int xOffset = -1 * Width / 2;

		std::vector<bool> emptyCells(Height * Width, false);

		for (auto i = 0; i < SnakeActor->SnakeElements.Num(); ++i)
		{
			auto &snakeCell = SnakeActor->SnakeElements[i]->CellLocation;
			if(snakeCell.Y * Width + snakeCell.X < emptyCells.size())
				emptyCells[ snakeCell.Y * Width + snakeCell.X ] = true;	
		}

		for (auto i = 0; i < ObstacleElements.Num(); ++i)
		{
			auto &obstacleCell = ObstacleElements[i]->CellLocation;
			emptyCells[obstacleCell.Y * Width + obstacleCell.X] = true;
		}

		auto availablePositions = emptyCells.size() - std::accumulate(emptyCells.cbegin(), emptyCells.cend(), 0);
		//auto availablePositions = emptyCells.size() - SnakeActor->SnakeElements.Num();

		auto position = FMath::RandRange(0, availablePositions - 1);
		
		int index = 0;
		for (; index < emptyCells.size(); ++index)
		{
			if (emptyCells[index])
				continue;

			if(-1 == --position)
				break;
		}

		int y = index / Width;
		int x = index % Width;

		FVector newLocation(SnakeActor->ElementSize * (y + yOffset), SnakeActor->ElementSize * (x + xOffset), 0.f);
		GetWorld()->SpawnActor<AFood>(FoodClass, FTransform(newLocation));
	}
	else
		GetWorld()->SpawnActor<AFood>(FoodClass, FTransform());
}

void APlayerPawnBase::AddScore(int value /*= 1*/)
{
	Score += value;
}

inline int APlayerPawnBase::CalculateLevelFoods(int level)
{
	return level * 2 + 4;
}

void APlayerPawnBase::AddObstacle()
{
	static int yOffset = -1 * Height / 2;
	static int xOffset = -1 * Width / 2;

	std::vector<bool> emptyCells(Height * Width, false);

	for (auto i = 0; i < SnakeActor->SnakeElements.Num(); ++i)
	{
		auto &snakeCell = SnakeActor->SnakeElements[i]->CellLocation;
		if (snakeCell.Y * Width + snakeCell.X < emptyCells.size())
			emptyCells[snakeCell.Y * Width + snakeCell.X] = true;
	}

	auto &snakeHeadLocation = SnakeActor->SnakeElements[0]->CellLocation;

	for (auto y = -2; y < 3; ++y)
		for (auto x = -2; x < 3; ++x)
			emptyCells[ FMath::Clamp(snakeHeadLocation.Y + y, 0, Height) * Width + FMath::Clamp(snakeHeadLocation.X + x, 0, Width)] = true;

	for (auto i = 0; i < ObstacleElements.Num(); ++i)
	{
		auto &obstacleCell = ObstacleElements[i]->CellLocation;

		for (auto y = -2; y < 3; ++y)
			for (auto x = -2; x < 3; ++x)
				emptyCells[FMath::Clamp(obstacleCell.Y + y, 0, Height) * Width + FMath::Clamp(obstacleCell.X + x, 0, Width)] = true;
	}

	auto availablePositions = emptyCells.size() - std::accumulate(emptyCells.cbegin(), emptyCells.cend(), 0);
	auto position = FMath::RandRange(0, availablePositions - 1);

	int index = 0;
	for (; index < emptyCells.size(); ++index)
	{
		if (emptyCells[index])
			continue;

		if (-1 == --position)
			break;
	}

	if (emptyCells.size() == index)
		return;

	int y = index / Width;
	int x = index % Width;

	FVector newLocation(SnakeActor->ElementSize * (y + yOffset), SnakeActor->ElementSize * (x + xOffset), 0.f);

	auto newObstacle = GetWorld()->SpawnActor<AObstacle>(ObstacleClass, FTransform(newLocation));
	newObstacle->CellLocation = FIntVector(x, y, 0);

	ObstacleElements.Add(newObstacle);
}
// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "SnakeGameModeBase.generated.h"

class UUserWidget;

/**
 * 
 */
UCLASS()
class TASK_21_API ASnakeGameModeBase : public AGameModeBase
{
	GENERATED_BODY()

public:
	virtual void BeginPlay() override;
	
	UPROPERTY(EditDefaultsOnly)
	TSubclassOf<UUserWidget> UserWidgetClass;

	UPROPERTY()
	UUserWidget *HUDInterace;
};

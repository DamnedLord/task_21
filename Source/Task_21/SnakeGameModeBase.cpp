// Fill out your copyright notice in the Description page of Project Settings.

#include "SnakeGameModeBase.h"

#include <Blueprint/UserWidget.h>


void ASnakeGameModeBase::BeginPlay()
{
	Super::BeginPlay();

	if (UserWidgetClass)
	{
		HUDInterace = CreateWidget<UUserWidget>(GetWorld(), UserWidgetClass);

		if (HUDInterace)
		{
			HUDInterace->AddToViewport();
		}
	}
}
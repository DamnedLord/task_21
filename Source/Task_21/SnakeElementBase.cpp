// Fill out your copyright notice in the Description page of Project Settings.

#include "SnakeElementBase.h"
#include "SnakeBase.h"
#include "PlayerPawnBase.h"

#include <Components/StaticMeshComponent.h>

// Sets default values
ASnakeElementBase::ASnakeElementBase()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	MeshComponent = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("MeshComponent"));
	MeshComponent->SetCollisionEnabled(ECollisionEnabled::QueryOnly);
	MeshComponent->SetCollisionResponseToAllChannels(ECollisionResponse::ECR_Overlap);

	IsHead = false;
}

// Called when the game starts or when spawned
void ASnakeElementBase::BeginPlay()
{
	Super::BeginPlay();
}

// Called every frame
void ASnakeElementBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void ASnakeElementBase::SetFirstElementType_Implementation()
{
	IsHead = true;
	MeshComponent->OnComponentBeginOverlap.AddDynamic(this, &ASnakeElementBase::HandleBeginOverlap);
}

void ASnakeElementBase::Interact(AActor * Interactor, bool bIsHead)
{
	auto Snake = Cast<ASnakeBase>(Interactor);
	if (!IsValid(Snake))
		return;

	Snake->Destroy();
	Snake->PlayerPawn->IsGameOver = true;
}

void ASnakeElementBase::HandleBeginOverlap(
	UPrimitiveComponent *OverlappedComponent,
	AActor *OtherActor,
	UPrimitiveComponent *OtherComponent,
	int32 OtherBodyIndex,
	bool bFromSweep,
	const FHitResult &SweepResult)
{
	if (!IsValid(SnakeOwner))
		return;

	SnakeOwner->SnakeElementOverlap(this, OtherActor);
}

void ASnakeElementBase::ToggleCollision()
{
	MeshComponent->SetCollisionEnabled(
		ECollisionEnabled::NoCollision == MeshComponent->GetCollisionEnabled()
		? ECollisionEnabled::QueryOnly
		: ECollisionEnabled::NoCollision
	);
}

